pipeline {

	agent {
		label 'Android'
	}

	options {
		buildDiscarder(logRotator(numToKeepStr: '25'))
        skipStagesAfterUnstable()
        deleteDir()
	}

	stages {

		stage('Compile') {
			sh './gradlew compileDebugSources'
		}

		stage('Unit test') {
            steps {
                sh './gradlew testDebugUnitTest testDebugUnitTest'

                junit '**/TEST-*.xml'
            }
        }

        stage('Build APK') {
            steps {
                sh './gradlew assembleDebug'

                archiveArtifacts '**/*.apk'
            }
        }

        stage('Static analysis') {
            steps {
                sh './gradlew lintDebug'
            }
        }

        stage('Deploy') {
            when {
                branch 'develop'
            }

            environment {
                 SIGNING_KEYSTORE = "my-app-signing-keystore"
                 SIGNING_KEY_PASSWORD = "my-app-signing-password"
            }

            steps {
                sh './gradlew assembleRelease'

                archiveArtifacts artifacts: '**/*.apk', followSymlinks: false, onlyIfSuccessful: true

                androidApkUpload deobfuscationFilesPattern: '**/build/outputs/**/mapping.txt', googleCredentialsId: 'AdsWatch', rolloutPercentage: '0', trackName: 'internal', usePreviousExpansionFilesIfMissing: true
            }
        }
	}

	post {
        failure {
            // mail bcc: '', body: 'Build ${env.BUILD_NUMBER} failed; ${env.BUILD_URL}', cc: '', from: '', replyTo: '', subject: 'Oops! build failed', to: 'wsservices29@gmail.com'
            echo 'Build failed!'
        }

        success {
            echo 'Build created successfully!'
        }
    }

}